package com.pointlessapps.sportly.asyncs;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.PlaylistSimple;
import kaaes.spotify.webapi.android.models.PlaylistTrack;
import kaaes.spotify.webapi.android.models.Track;

public class GetAllTracks extends AsyncTask<Void, Void, List<Track>> {

	private SpotifyService spotifyService;
	private TracksListener tracksListener;

	public GetAllTracks(SpotifyService spotifyService, TracksListener tracksListener) {
		this.spotifyService = spotifyService;
		this.tracksListener = tracksListener;
	}

	@Override
	protected List<Track> doInBackground(Void... voids) {
		List<Track> tracks = new ArrayList<>();
		List<PlaylistSimple> items = spotifyService.getMyPlaylists().items;
		String id = spotifyService.getMe().id;
		for(PlaylistSimple p : items) {
			List<PlaylistTrack> playlistTracks = spotifyService.getPlaylistTracks(id, p.id).items;
			for(PlaylistTrack playlistTrack : playlistTracks) {
				tracks.add(playlistTrack.track);
			}
		}

		return tracks;
	}

	@Override
	protected void onPostExecute(List<Track> tracks) {
		tracksListener.getTracks(tracks);
	}

	public interface TracksListener {
		void getTracks(List<Track> tracks);
	}
}