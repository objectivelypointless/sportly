package com.pointlessapps.sportly.asyncs;

import android.os.AsyncTask;

import java.util.List;

import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.PlaylistSimple;

public class GetAllPlaylists extends AsyncTask<Void, Void, List<PlaylistSimple>> {

	private SpotifyService spotifyService;
	private PlaylistsListener playlistsListener;

	public GetAllPlaylists(SpotifyService spotifyService, PlaylistsListener playlistsListener) {
		this.spotifyService = spotifyService;
		this.playlistsListener = playlistsListener;
	}

	@Override
	protected List<PlaylistSimple> doInBackground(Void... voids) {
		return spotifyService.getMyPlaylists().items;
	}

	@Override
	protected void onPostExecute(List<PlaylistSimple> playlists) {
		playlistsListener.getPlaylists(playlists);
	}

	public interface PlaylistsListener {
		void getPlaylists(List<PlaylistSimple> playlistSimples);
	}
}
