package com.pointlessapps.sportly.asyncs;

import android.os.AsyncTask;

import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.AudioFeaturesTracks;

public class GetTracksFeatures extends AsyncTask<Void, Void, AudioFeaturesTracks> {

    private String id;
    private TrackFeaturesListener trackFeaturesListener;
    private SpotifyService spotifyService;

    public GetTracksFeatures(SpotifyService spotifyService, String id, TrackFeaturesListener trackFeaturesListener) {
        this.spotifyService = spotifyService;
        this.id = id;
        this.trackFeaturesListener = trackFeaturesListener;
    }

    @Override
    protected AudioFeaturesTracks doInBackground(Void... voids) {
        return spotifyService.getTracksAudioFeatures(id);
    }

    @Override
    protected void onPostExecute(AudioFeaturesTracks featuresTrack) {
        trackFeaturesListener.getTrackFeatures(featuresTrack);
    }

    public interface TrackFeaturesListener {
        void getTrackFeatures(AudioFeaturesTracks featuresTrack);
    }
}
