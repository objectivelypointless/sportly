package com.pointlessapps.sportly.asyncs;

import android.os.AsyncTask;

import java.util.List;

import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.Track;

public class GetTrack extends AsyncTask<Void, Void, Track> {

    private String id;
    private TrackListener trackListener;
    private SpotifyService spotifyService;

    public GetTrack(SpotifyService spotifyService, String id, TrackListener trackListener) {
        this.spotifyService = spotifyService;
        this.id = id;
        this.trackListener = trackListener;
    }

    @Override
    protected Track doInBackground(Void... voids) {
        return spotifyService.getTrack(id);
    }

    @Override
    protected void onPostExecute(Track track) {
        trackListener.getTrack(track);
    }

	public interface TrackListener {
		void getTrack(Track track);
	}
}
