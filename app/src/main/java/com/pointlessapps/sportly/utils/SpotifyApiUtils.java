package com.pointlessapps.sportly.utils;

import com.pointlessapps.sportly.asyncs.GetAllTracks;
import com.pointlessapps.sportly.asyncs.GetTrack;
import com.pointlessapps.sportly.asyncs.GetTracksFeatures;

import java.util.ArrayList;
import java.util.List;

import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.AudioFeaturesTrack;
import kaaes.spotify.webapi.android.models.Track;

public class SpotifyApiUtils {

    private static SpotifyApiUtils instance = getInstance();
    private List<Track> tracksByTempo;
    private SpotifyService spotifyService;

    public static SpotifyApiUtils getInstance() {
        return instance == null ? instance = new SpotifyApiUtils() : instance;
    }

    public SpotifyApiUtils setSpotifyService(SpotifyService spotifyService) {
        this.spotifyService = spotifyService;
        return this;
    }

    public void getTracksByTempo(long tempo, GetAllTracks.TracksListener tracksListener) {
        tracksByTempo = new ArrayList<>();
        new GetAllTracks(spotifyService, tracks -> {
            StringBuilder stringBuilder = new StringBuilder();
            for(int i = 0; i < tracks.size() && i < 75; i++) {
                Track t = tracks.get(i);
                stringBuilder.append(t.id).append(",");
            }
            stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
            new GetTracksFeatures(spotifyService, stringBuilder.toString(), featuresTrack -> {
                List<AudioFeaturesTrack> tempTempo = featuresTrack.audio_features;
                int counter = 0;
                for (AudioFeaturesTrack aFT : tempTempo)
                    if (aFT != null && Math.abs(aFT.tempo - tempo) <= 10) {
                        counter++;
                        int finalCounter = counter;
                        new GetTrack(spotifyService, aFT.id, track -> {
                            tracksByTempo.add(track);
                            if (tracksByTempo.size() == finalCounter)
                                tracksListener.getTracks(tracksByTempo);
                        }).execute();
                    }


            }).execute();
        }).execute();
    }
}