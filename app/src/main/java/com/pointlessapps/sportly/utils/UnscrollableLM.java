package com.pointlessapps.sportly.utils;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;

public class UnscrollableLM extends GridLayoutManager {

	public UnscrollableLM(Context context, int spanCount, int orientation, boolean reverseLayout) {
		super(context, spanCount, orientation, reverseLayout);
	}

	@Override
	public boolean canScrollVertically() {
		return false;
	}

	@Override public boolean canScrollHorizontally() {
		return false;
	}
}
