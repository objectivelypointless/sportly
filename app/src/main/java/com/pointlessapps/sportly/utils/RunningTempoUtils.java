package com.pointlessapps.sportly.utils;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class RunningTempoUtils implements SensorEventListener {

    private static RunningTempoUtils instance = getInstance();
    private final long TIME_THRESHOLD = TimeUnit.SECONDS.toMillis(20);
    private ArrayList<Long> timeTable;
    private TempoChangeListener tempoChangeListener;

    public static RunningTempoUtils getInstance() {
        return instance == null ? instance = new RunningTempoUtils() : instance;
    }

    private long getTempo(long avgStepTime) {
        return TimeUnit.MINUTES.toMillis(1) / avgStepTime;
    }

    public void registerSensorService(Context context, TempoChangeListener tempoChangeListener) {
        this.tempoChangeListener = tempoChangeListener;
        timeTable = new ArrayList<>();
        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    private Long getAvgTimeDiff() {
        int tableSize = timeTable.size() - 1;
        long topIndexTime = timeTable.get(tableSize);
        long avgTime = 0;
        for (int i = tableSize - 1; i >= 0; i--) {
            long diff = topIndexTime - timeTable.get(i);
            int amtOfElements = tableSize - i + 1;
            long twoElemsDiff = timeTable.get(i + 1) - timeTable.get(i);
            avgTime = (avgTime * amtOfElements + twoElemsDiff) / (amtOfElements + 1);

            if (diff >= TIME_THRESHOLD) {
                removeElements(i - 1);
                break;
            }
        }
        return avgTime;
    }

    private void removeElements(int index) {
        if (index >= 0) timeTable.subList(0, index + 1).clear();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {

            long timeInMills = (System.currentTimeMillis() + (event.timestamp - System.nanoTime()) / 1000000L);
            timeTable.add(timeInMills);
            if (timeTable.size() > 1) {
                long tempo = getTempo(getAvgTimeDiff());
                tempoChangeListener.tempoChanged(tempo);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public interface TempoChangeListener {
        void tempoChanged(long tempo);
    }
}
