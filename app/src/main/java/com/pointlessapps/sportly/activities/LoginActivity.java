package com.pointlessapps.sportly.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.pointlessapps.sportly.R;
import com.pointlessapps.sportly.utils.SpotifyApiUtils;
import com.spotify.android.appremote.api.ConnectionParams;
import com.spotify.android.appremote.api.Connector;
import com.spotify.android.appremote.api.SpotifyAppRemote;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;

import java.util.List;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.Track;

public class LoginActivity extends Activity {

    private static final String CLIENT_ID = "2a340a7a661641678561211a0147e0eb";
    private static final String REDIRECT_URI = "festevo://callback";
    private static final int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        connect();
    }

    public void clickConnect(View view) {
        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);

        builder.setScopes(new String[]{"streaming", "app-remote-control", "playlist-read-private"});
        AuthenticationRequest request = builder.build();

        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, data);

            switch (response.getType()) {
                case TOKEN:
                    String accessToken = response.getAccessToken();
                    getSharedPreferences("TOKEN", MODE_PRIVATE).edit().putString("token", accessToken).apply();
                    connect();
                    break;
                case ERROR:
                    Log.d("elo", response.getError());
                    break;
                default:
            }
        }
    }

    private void connect() {
	    findViewById(R.id.progressBar).setVisibility(View.VISIBLE);

        SpotifyAppRemote.connect(
                getApplication(),
                new ConnectionParams.Builder(CLIENT_ID)
                        .setRedirectUri(REDIRECT_URI)
                        .showAuthView(false)
                        .build(),
                new Connector.ConnectionListener() {
                    @Override
                    public void onConnected(SpotifyAppRemote spotifyAppRemote) {
                        startActivity(new Intent(getApplicationContext(), PlaylistsActivity.class));
                        finish();
                    }

                    @Override
                    public void onFailure(Throwable error) {
                        Toast.makeText(getApplicationContext(), String.format("Connection failed: %s", error), Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
