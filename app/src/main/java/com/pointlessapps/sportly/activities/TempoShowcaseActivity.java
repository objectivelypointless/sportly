package com.pointlessapps.sportly.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;

import com.pointlessapps.sportly.R;
import com.pointlessapps.sportly.utils.RunningTempoUtils;

public class TempoShowcaseActivity extends Activity {

	private int setTempo;
	private String[] texts = {"You're doing great", "Great tempo, see if you can raise it!"};

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tempo);

		setTempo = getSharedPreferences("TEMPO", MODE_PRIVATE).getInt("setTempo", 0);
		RunningTempoUtils.getInstance().registerSensorService(this, tempo -> {
			((AppCompatTextView)findViewById(R.id.currentTempoText)).setText(String.valueOf(tempo));
			if(tempo >= setTempo - 10) ((AppCompatTextView) findViewById(R.id.descriptionText)).setText(texts[0]);
			else ((AppCompatTextView) findViewById(R.id.descriptionText)).setText(texts[1]);
		});
	}
}
