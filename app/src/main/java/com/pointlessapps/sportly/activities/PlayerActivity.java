package com.pointlessapps.sportly.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.View;

import com.pointlessapps.sportly.R;
import com.pointlessapps.sportly.adapters.TracksListAdapter;
import com.pointlessapps.sportly.utils.SpotifyApiUtils;

import java.util.ArrayList;
import java.util.List;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.Track;

public class PlayerActivity extends Activity {

	private int currentTempo = 120;
	private List<Track> tracks;
	private TracksListAdapter tracksListAdapter;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player);

		RecyclerView tracksList = findViewById(R.id.tracksList);
		tracksList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
		tracksList.setAdapter(tracksListAdapter = new TracksListAdapter(tracks = new ArrayList<>()));

		getPlaylists();
	}

	private void getPlaylists() {
		String accessToken = getSharedPreferences("TOKEN", MODE_PRIVATE).getString("token", "");

		SpotifyApi api = new SpotifyApi();
		api.setAccessToken(accessToken);
		SpotifyService spotifyService = api.getService();
		SpotifyApiUtils.getInstance().setSpotifyService(spotifyService).getTracksByTempo(currentTempo, tracks -> {
			PlayerActivity.this.tracks.clear();
			PlayerActivity.this.tracks.addAll(tracks);
			tracksListAdapter.notifyDataSetChanged();
			TransitionManager.beginDelayedTransition(findViewById(R.id.background), new AutoTransition());
		});
	}

	public void clickLeftArrow(View view) {
		currentTempo -= 5;
		((AppCompatTextView) findViewById(R.id.currentTempoText)).setText(String.valueOf(currentTempo));
		getPlaylists();
	}

	public void clickRightArrow(View view) {
		currentTempo += 5;
		((AppCompatTextView) findViewById(R.id.currentTempoText)).setText(String.valueOf(currentTempo));
		getPlaylists();
	}

	public void clickTempoShowcase(View view) {
		getSharedPreferences("TEMPO", MODE_PRIVATE).edit().putInt("setTempo", currentTempo).apply();
		startActivity(new Intent(this, TempoShowcaseActivity.class));
		finish();
	}
}
