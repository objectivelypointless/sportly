package com.pointlessapps.sportly.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.pointlessapps.sportly.R;
import com.pointlessapps.sportly.adapters.PlaylistsAdapter;
import com.pointlessapps.sportly.asyncs.GetAllPlaylists;
import com.pointlessapps.sportly.utils.UnscrollableLM;

import kaaes.spotify.webapi.android.SpotifyApi;

public class PlaylistsActivity extends Activity {

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_playlists);

		setPlaylistsList();
	}

	private void setPlaylistsList() {
		String accessToken = getSharedPreferences("TOKEN", MODE_PRIVATE).getString("token", "");

		SpotifyApi api = new SpotifyApi();
		api.setAccessToken(accessToken);

		new GetAllPlaylists(api.getService(), playlistSimples -> {
			RecyclerView playlistsList = findViewById(R.id.playlistsList);
			PlaylistsAdapter playlistsAdapter = new PlaylistsAdapter(playlistSimples);
			playlistsAdapter.setOnClickListener(position -> {
				Log.d("LOG!", "clicked at: " + position);
			});
			playlistsList.setAdapter(playlistsAdapter);
			playlistsList.setLayoutManager(new UnscrollableLM(getApplicationContext(), 2, LinearLayoutManager.VERTICAL, false));
		}).execute();
	}
}