package com.pointlessapps.sportly.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.pointlessapps.sportly.R;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
