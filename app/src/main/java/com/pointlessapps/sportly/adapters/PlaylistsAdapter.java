package com.pointlessapps.sportly.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pointlessapps.sportly.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import kaaes.spotify.webapi.android.models.PlaylistSimple;

public class PlaylistsAdapter extends RecyclerView.Adapter<PlaylistsAdapter.DataObjectHolder> {

	private final List<PlaylistSimple> playlists;
	private ClickListener clickListener;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		AppCompatTextView name;
		AppCompatImageView thumbnail;

		DataObjectHolder(View itemView) {
			super(itemView);
			name = itemView.findViewById(R.id.name);
			thumbnail = itemView.findViewById(R.id.thumbnail);

			itemView.findViewById(R.id.bg).setOnClickListener(v -> clickListener.onPlaylistClick(getAdapterPosition()));
		}
	}

	public PlaylistsAdapter(List<PlaylistSimple> tracks) {
		this.playlists = tracks;
		setHasStableIds(true);
	}

	public void setOnClickListener(ClickListener clickListener) {
		this.clickListener = clickListener;
	}

	@Override
	public long getItemId(int position) {
		return playlists.get(position).hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.playlists_list_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.name.setText(playlists.get(position).name);
		if(!playlists.get(position).images.isEmpty())
			Picasso.get().load(playlists.get(position).images.get(0).url).into(holder.thumbnail);
	}

	@Override
	public int getItemCount() {
		return playlists.size();
	}

	public interface ClickListener {
		void onPlaylistClick(int position);
	}
}
