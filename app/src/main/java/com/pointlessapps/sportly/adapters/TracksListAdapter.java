package com.pointlessapps.sportly.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pointlessapps.sportly.R;

import java.util.List;
import java.util.Locale;

import kaaes.spotify.webapi.android.models.Track;

public class TracksListAdapter extends RecyclerView.Adapter<TracksListAdapter.DataObjectHolder> {

	private final List<Track> tracks;

	class DataObjectHolder extends RecyclerView.ViewHolder {
		AppCompatTextView title, subtitle, time;

		DataObjectHolder(View itemView) {
			super(itemView);
			title = itemView.findViewById(R.id.title);
			subtitle = itemView.findViewById(R.id.subtitle);
			time = itemView.findViewById(R.id.time);
		}
	}

	public TracksListAdapter(List<Track> tracks) {
		this.tracks = tracks;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return tracks.get(position).hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.track_list_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.title.setText(tracks.get(position).name);
		holder.subtitle.setText(tracks.get(position).artists.get(0).name);
		holder.time.setText(String.format(Locale.getDefault(), "%tM:%tS", tracks.get(position).duration_ms, tracks.get(position).duration_ms));
	}

	@Override
	public int getItemCount() {
		return tracks.size();
	}
}